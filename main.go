package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
)

type Pair struct {
	A, B rune
}

func main() {
	flag.Parse()

	count := map[Pair]int{}

	args := flag.Args()

	for _, arg := range args {
		var r io.ReadCloser
		var err error

		if arg == "-" {
			r = os.Stdin
		} else {
			r, err = os.Open(arg)
			if err != nil {
				log.Fatal(err)
			}
			defer r.Close()
		}

		Count(r, count)
	}

	for p, c := range count {
		if c > 0 {
			fmt.Printf("%s %U %U %d\n",
				string([]rune{p.A, p.B}), p.A, p.B, count[p])
		}
	}
}

func Count(reader io.Reader, count map[Pair]int) {
	in := bufio.NewReader(reader)

	p := rune(-1)

	for {
		r, _, err := in.ReadRune()
		if err != nil {
			break
		}
		count[Pair{p, r}] += 1
		p = r
	}
}
